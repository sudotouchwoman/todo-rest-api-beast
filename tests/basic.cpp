#include <gtest/gtest.h>

TEST(BasicTest, BasicAssertions) {
  EXPECT_STRNE("foo", "bar");
  EXPECT_EQ(100, 200 / 2);
}

int main(int n_args, char* args[]) {
  ::testing::InitGoogleTest(&n_args, args);
  return RUN_ALL_TESTS();
}
