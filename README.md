# **Basic REST API with Boost Beast**

Currently, only slightly modified example of async fileserver from `boost::beast` library is
present, but I am now working on extending it to a more sophisticated API.

Build:

```bash
mkdir -p build && cd build
cmake .. && make
```

Run the server on port 8080

```bash
cd build/project
./server 127.0.0.1 8080 $(nproc)
```

You can now create connections to the server, e.g. using `curl`:

```bash
curl -v "http://localhost:5000/todos"
curl -v "http://localhost:5000/todo?id=todo_id"
curl -v "http://localhost:5000/my?uid=user_id"
curl -v 'http://localhost:5000/recent?uid=user_id&ts=1684596000"
```

In current implementation all data is stored in RAM
(simply using `std::unordered_map`), hence the server has no data on start. One can post some todos using the following payload syntax:

```bash
curl -v http://localhost:5000/add -d '{
  "id": "todo_id",
  "created_by": "user_id",
  "created_at": "2023-05-20T15:22:18Z",
  "updated_at": "2023-05-20T15:22:18Z",
  "content": "Handle partial jsons for post request",
  "tags": [
    "beast",
    "boost",
    "CXX",
    "important"
  ]
}'
```

And then query server for uploaded data.
Todo with specific id can be deleted like this:

```bash
curl -v "http://localhost:5000/remove?id=todo_id"
```

Server can be gracefully stopped by sending `SIGINT` (`^C`) or `SIGTERM` (`kill` command).

Yeah, it its current shape, it is hard to find a more insecure and cumbersome API. However, I consider it to be a good starting point for
C++ networking expierence. Some refactoring points are:

+ Naming consistency. Some class names have lowercase names (`listener`, `http_session`) while other use upper camel (`TodoProvider`, `HttpHandler`). The former relate to `boost::asio` or `boost::beast` libraries, though.
+ Checking request type in `BasicRouter`. Currently, several handlers
can't be used for the same endpoint but different request types (as matching is only being performed based on relative path).
+ Fixing the templating of `HandlerFunc` and `Handler` types: they were designed to handle different request types as the `boost::beast::http::request` is a template class. Interfaces and templates should be combined with caution as they refer to different ways to achieve polymorphism.
+ Magic strings in handler methods of `TodoHandler` used for error handling.
