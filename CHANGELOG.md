# **Changelog**

## **2023-05-17**
### Notes
+ Toyed around with `boost::beast` and `boost::asio`. The latter
caused some problems as example from the docs did not basically compile with C++17
(due to `<coroutine>`). This feels weird as boost should have and support its own
version of coroutines (it is likely where STL implementation originated from). Beast
example compiled fine, however. This is the first time I am working closely with `boost`,
yet most of the concepts feel familiar from other programming languages (like Golang or Python).
Planning to implement a basic JSON API to be later consumed by different microservice.
+ Configured CMake manifests to properly include and link boost components.
+ Got a `clang-format` configuration file.
+ CMake integration for VSCode feels better but still somewhat laggy
(warnings and errors will not go away until recompilation and the squiggles might get disturbing).

## **2023-05-18**
### Notes
+ Pushed the repo to Gitlab, merged changes to `main` branch, completed my first non-trivial rebase.
+ Created a `Dockerfile` for `boost 1.81.0` installation from sources on top of `ubuntu:22.04` image.

## **2023-05-20**
### Notes
+ Installed Gitlab runner with docker executor.
Configured it to run automatically on server reboots.
+ Pushed image with boost dependencies to `registry.gitlab.com/sudotouchwoman/todo-rest-api-beast/devcontainer-boost`
+ Set up simple CI job to build the application on pushes. Now testing and linting jobs can be set up too.
+ Implemented a basic json-based API with `boost`. It can currently store data in RAM and handle `GET` requests to fetch or post data to the server. Data model is a simple list of Todo objects, see details in `README.md`.
