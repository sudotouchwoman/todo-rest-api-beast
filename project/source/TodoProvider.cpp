#include "TodoProvider.hpp"
#include <iostream>

namespace errc = boost::system::errc;

// https://stackoverflow.com/questions/51263053/no-out-of-line-virtual-method-definitions-for-full-specialized-template-class
TodoProvider::~TodoProvider() = default;

// Serialize TodoItem into a boost::json::value object
json::value TodoItem::asJson() const {
  return {
      {"id", id},
      {"created_by", created_by},
      {"created_at", ts_to_isotime(created_at)},
      {"updated_at", ts_to_isotime(updated_at)},
      {"content", content},
      {"tags",
       [](std::vector<std::string> const& tags) {
         json::array tag_list;
         tag_list.reserve(tags.size());
         for (auto const& tag : tags) {
           tag_list.emplace_back(tag);
         }
         return tag_list;
       }(tags)},
  };
}

beast::error_code fromJson(TodoItem& t, json::value const& s) {
  // todo id must be a string
  // https://theboostcpplibraries.com/boost.system
  if (not s.at_pointer("/id").is_string())
    return errc::make_error_code(errc::bad_message);
  // author id must be a string
  if (not s.at_pointer("/created_by").is_string())
    return errc::make_error_code(errc::bad_message);
  // refactoring candidates
  // creation must be a valid iso string
  if (not s.at_pointer("/created_at").is_string() or
      not is_iso_string(std::string(s.at_pointer("/created_at").as_string()))) {
    return errc::make_error_code(errc::bad_message);
  }
  // updated_at must be a string
  if (not s.at_pointer("/updated_at").is_string() or
      not is_iso_string(std::string(s.at_pointer("/created_at").as_string()))) {
    return errc::make_error_code(errc::bad_message);
  }
  // Content must be a string
  if (not s.at_pointer("/content").is_string())
    return errc::make_error_code(errc::bad_message);
  // Tags must be an array
  if (not s.at_pointer("/tags").is_array()) {
    return errc::make_error_code(errc::bad_message);
  }
  // All tags must be valid strings
  for (auto const& tag : s.at_pointer("/tags").as_array()) {
    if (tag.is_string())
      continue;
    return errc::make_error_code(errc::bad_message);
  }

  // Construct the todo item from JSON attributes
  t.id = std::move(s.at_pointer("/id").as_string());
  t.content = std::move(s.at_pointer("/content").as_string());
  t.created_by = std::move(s.at_pointer("/created_by").as_string());
  t.created_at =
      isotime_to_ts(std::string(s.at_pointer("/created_at").as_string()));
  t.updated_at =
      isotime_to_ts(std::string(s.at_pointer("/updated_at").as_string()));
  // Parse tag list
  t.tags = [&s]() {
    std::vector<std::string> tags;
    auto const tags_array = s.at_pointer("/tags").as_array();
    tags.reserve(tags_array.size());
    // Refactored range-based foor loop with std::transform
    std::transform(tags_array.cbegin(), tags_array.cend(),
                   std::back_inserter(tags),
                   [](json::value const& tag) -> std::string {
                     return std::string(tag.as_string());
                   });
    return tags;
  }();
  return {};
}

std::pair<TodoList, beast::error_code> InMemoryTodoProvider::allTodos() const {
  std::lock_guard<std::mutex> lg(mu_);

  TodoList tl;
  tl.reserve(todos_map_.size());
  for (auto const& [id, todo] : todos_map_) {
    tl.emplace_back(todo);
  }
  return {tl, {}};
}

std::pair<TodoItem, beast::error_code> InMemoryTodoProvider::todoByID(
    std::string const& id) const {
  std::lock_guard<std::mutex> lg(mu_);
  if (not todos_map_.count(id)) {
    // This id does not exist
    return {{}, errc::make_error_code(errc::address_not_available)};
  }
  // somehow [id] does not work here
  return {todos_map_.at(id), {}};
}

std::pair<TodoList, beast::error_code> InMemoryTodoProvider::todosByAuthor(
    std::string const& uid) const {
  std::lock_guard<std::mutex> lg(mu_);
  TodoList tl;
  for (auto const& [id, todo] : todos_map_) {
    if (todo.created_by == uid) {
      tl.emplace_back(todo);
    }
  }
  return {tl, {}};
}

std::pair<TodoList, beast::error_code>
InMemoryTodoProvider::todosByAuthorRecent(std::string const& uid,
                                          std::chrono::seconds const ts) const {
  std::lock_guard<std::mutex> lg(mu_);
  TodoList tl;
  for (auto const& [id, todo] : todos_map_) {
    // filtering by date is faster than by uid,
    // hence the order
    if (todo.created_at > ts and todo.created_by == uid) {
      tl.emplace_back(todo);
    }
  }
  return {tl, {}};
}

beast::error_code InMemoryTodoProvider::addTodo(TodoItem&& t) {
  std::lock_guard<std::mutex> lg(mu_);
  auto const id = t.id;
  if (todos_map_.count(id)) {
    // todo with such id already exists
    return errc::make_error_code(errc::bad_address);
  }

  todos_map_.emplace(std::move(id), std::move(t));
  return {};
}

beast::error_code InMemoryTodoProvider::removeByID(std::string const& id) {
  std::lock_guard<std::mutex> lg(mu_);
  if (not todos_map_.count(id)) {
    // todo with such id does not exist
    return errc::make_error_code(errc::address_not_available);
  }
  todos_map_.erase(id);
  return {};
}

// Implementation from
// https://en.cppreference.com/w/cpp/container/unordered_map/erase_if
// which only got in std by C++ 20
template <class T, class Pred>
std::size_t erase_if(T& c, Pred const& pred) {
  auto const old_size = c.size();
  for (auto i = c.begin(), last = c.end(); i != last;) {
    if (pred(*i)) {
      i = c.erase(i);
    } else {
      ++i;
    }
  }
  return old_size - c.size();
}

beast::error_code InMemoryTodoProvider::removeByAuthor(std::string const& uid) {
  std::lock_guard<std::mutex> lg(mu_);
  erase_if(todos_map_, [&uid](std::pair<std::string, TodoItem> const& pair) {
    return pair.second.created_by == uid;
  });

  return {};
}
