#include "TimeUtils.hpp"

#include <sstream>

#include <boost/regex.hpp>

// Converts time since epoch to ISO-string
// https://stackoverflow.com/questions/68046787/convert-stdchronomilliseconds-to-iso-8601-string
std::string ts_to_isotime(std::chrono::seconds const& secs) {
  return date::format("%FT%TZ", date::sys_time<std::chrono::seconds>{secs});
}

// Converts given ISO-string to seconds since epoch
std::chrono::seconds isotime_to_ts(std::string const& s) {
  std::istringstream in{s};
  date::sys_seconds tp;
  in >> date::parse("%FT%TZ", tp);
  if (in.fail()) {
    in.clear();
    in.str(s);
    in >> date::parse("%FT%T%z", tp);
  }
  return tp.time_since_epoch();
}

// Check whether given string is ISO-format compliant
// https://stackoverflow.com/questions/3143070/regex-to-match-an-iso-8601-datetime-string
bool is_iso_string(std::string const& x) {
  const boost::regex e(
      "^\\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12]\\d|3[01])T(?:[01]\\d|2[0-3]):["
      "0-5]"
      "\\d:[0-5]\\d(?:Z|-0[1-9]|-1\\d|-2[0-3]|-00:?(?:0[1-9]|[1-5]\\d)|\\+[01]"
      "\\d|"
      "\\+2[0-3])(?:|:?[0-5]\\d)$");
  return boost::regex_match(x, e);
}
