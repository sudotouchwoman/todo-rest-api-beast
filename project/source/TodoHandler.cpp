#include "TodoHandler.hpp"
#include <boost/url.hpp>

namespace urls = boost::urls;
namespace errc = boost::system::errc;

http::message_generator not_found_handler(
    http::request<http::string_body>&& req) {
  http::response<http::string_body> res{http::status::not_found, req.version()};
  res.set(http::field::server, SERVER_VERSION);
  res.set(http::field::content_type, "text/html");
  res.keep_alive(req.keep_alive());
  res.body() = "Page not found: ";
  res.body() += req.target();
  res.prepare_payload();
  return res;
}

// Returns a bad request response
http::message_generator bad_request(
    std::string_view why, http::request<http::string_body>&& req,
    http::status status = http::status::bad_request) {
  http::response<http::string_body> res{status, req.version()};
  res.set(http::field::server, SERVER_VERSION);
  res.set(http::field::content_type, "text/html");
  res.keep_alive(req.keep_alive());
  res.body() = std::string(why);
  res.prepare_payload();
  return res;
}

std::string serialize_todo_list(TodoList const& tl) {
  json::array a;
  a.reserve(tl.size());
  // Refactored range-based foor loop with std::transform
  std::transform(tl.cbegin(), tl.cend(), std::back_inserter(a),
                 [](TodoItem const& t) -> json::value { return t.asJson(); });
  return json::serialize(a);
}

// TODO(sudotouchwoman): implement all methods of TodoHandler
// GET /todos
http::message_generator TodoHandler::handleGetAllTodos(
    http::request<http::string_body>&& req) {
  auto const& [todos, ec] = provider_.allTodos();
  if (ec)
    return bad_request("Inavailable entry", std::move(req));

  http::response<http::string_body> res{http::status::ok, req.version()};
  // Dump all todos into response body as json array
  // Set the content length to actual payload size and
  // move the serialized buffer into response body buffer
  auto serialized = serialize_todo_list(todos);
  const auto size = serialized.size();
  res.set(http::field::content_type, "application/json");
  res.content_length(size);
  res.body() = std::move(serialized);
  return res;
}

// GET /todo?id=id_string
http::message_generator TodoHandler::handleGetTodoByID(
    http::request<http::string_body>&& req) {
  auto const u = urls::parse_relative_ref(req.target());
  if (u.has_error())
    return bad_request(u.error().message(), std::move(req));

  // At this point, url has been parsed succesfully
  const std::string id = [&u]() {
    auto const query_params = u.value().params();
    auto const it = query_params.find_last("id");
    return (*it)->value;
  }();

  if (id == "")
    return bad_request("Id required", std::move(req),
                       http::status::unprocessable_entity);

  // Fetch todo with given ID
  auto const& [todo, ec] = provider_.todoByID(id);
  if (ec == boost::system::errc::address_not_available) {
    std::stringstream why;
    why << "Entry not found: " << std::move(id);
    // such entry was not found
    return bad_request(why.str(), std::move(req), http::status::not_found);
  }

  if (ec)
    return bad_request(ec.what(), std::move(req),
                       http::status::internal_server_error);

  http::response<http::string_body> res{http::status::ok, req.version()};
  // Dump all todos into response body as json array
  auto serialized = json::serialize(todo.asJson());
  auto const size = serialized.size();
  res.set(http::field::content_type, "application/json");
  res.content_length(size);
  res.body() = std::move(serialized);
  return res;
}

// GET /my_todos?uid=id_string
http::message_generator TodoHandler::handleGetTodosByAuthor(
    http::request<http::string_body>&& req) {
  auto const u = urls::parse_relative_ref(req.target());
  if (u.has_error())
    return bad_request(u.error().message(), std::move(req));

  // At this point, url has been parsed succesfully
  const std::string uid = [&u]() {
    auto const query_params = u.value().params();
    auto const it = query_params.find_last("uid");
    return (*it)->value;
  }();

  if (uid == "")
    return bad_request("User id required", std::move(req));

  // Get all todos created by user with this id
  auto const& [todos, ec] = provider_.todosByAuthor(uid);
  if (ec)
    return bad_request("Inavailable entry", std::move(req));

  http::response<http::string_body> res{http::status::ok, req.version()};
  // Dump all todos into response body as json array
  auto serialized = serialize_todo_list(todos);
  auto const size = serialized.size();
  res.set(http::field::content_type, "application/json");
  res.content_length(size);
  res.body() = std::move(serialized);
  return res;
}

// GET /my_todos?uid=id_string&ts=1440675100
http::message_generator TodoHandler::handleGetTodosAuthorRecent(
    http::request<http::string_body>&& req) {
  auto const u = urls::parse_relative_ref(req.target());
  if (u.has_error())
    return bad_request(u.error().message(), std::move(req));

  // At this point, url has been parsed succesfully
  const std::string uid = [&u]() {
    auto const query_params = u.value().params();
    auto const it = query_params.find_last("uid");
    return (*it)->value;
  }();

  if (uid == "")
    return bad_request("Non-empty user id is required", std::move(req));

  auto const ts = [&u]() {
    auto const query_params = u.value().params();
    auto const it = query_params.find_last("ts");
    return std::chrono::seconds(std::atol((*it)->value.c_str()));
  }();

  if (ts.count() == 0)
    return bad_request("Invalid timestamp", std::move(req));

  // Get all todos created by user with this id
  auto const& [todos, ec] = provider_.todosByAuthorRecent(uid, ts);
  if (ec)
    return bad_request("Inavailable entry", std::move(req));

  http::response<http::string_body> res{http::status::ok, req.version()};
  // Dump all todos into response body as json array
  auto serialized = serialize_todo_list(todos);
  auto const size = serialized.size();
  res.set(http::field::content_type, "application/json");
  res.content_length(size);
  res.body() = std::move(serialized);
  return res;
}

// Note: this endpoint feels a bit off as user id and new todo id
// are either not validated or simply discarded
// POST /add_todo
http::message_generator TodoHandler::handleAddTodo(
    http::request<http::string_body>&& req) {

  // Create placeholder for a todo, fill it with values
  // from parsed JSON
  TodoItem t{};
  json::value todo_json;
  beast::error_code jsonec;
  // json parsing cat throw if provided body is not
  // a valid json
  try {
    todo_json = json::parse(req.body());
    jsonec = fromJson(t, todo_json);
  } catch (boost::system::system_error const& err) {
    // something went wrong during parsing
    jsonec = errc::make_error_code(errc::bad_message);
  }
  if (jsonec)
    return bad_request("Invalid payload format", std::move(req),
                       http::status::unprocessable_entity);

  // Pass newly created Todo entry to the provider
  const auto& ec = provider_.addTodo(std::move(t));
  if (ec == boost::system::errc::bad_address)
    // this entry already exists
    return bad_request("Already exists", std::move(req),
                       http::status::bad_request);

  if (ec)
    return bad_request("Resource inavailable", std::move(req),
                       http::status::internal_server_error);

  http::response<http::string_body> res{http::status::created, req.version()};
  res.set(http::field::content_type, "application/json");
  res.prepare_payload();
  return res;
}

// DELETE /todo?id=id_string
http::message_generator TodoHandler::handleRemoveTodoByID(
    http::request<http::string_body>&& req) {
  auto const u = urls::parse_relative_ref(req.target());
  if (u.has_error())
    return bad_request(u.error().message(), std::move(req));

  // At this point, url has been parsed succesfully
  const std::string id = [&u]() {
    auto const query_params = u.value().params();
    auto const it = query_params.find_last("id");
    return (*it)->value;
  }();

  if (id == "")
    return bad_request("Id required", std::move(req));

  auto const& ec = provider_.removeByID(id);
  if (ec == boost::system::errc::address_not_available)
    return bad_request("Entry does not exist", std::move(req));
  if (ec)
    return bad_request("Inavailable entry", std::move(req));

  http::response<http::string_body> res{http::status::ok, req.version()};
  res.prepare_payload();
  return res;
}

http::message_generator BasicRouter::handle(
    http::request<http::string_body>&& req) const {
  const urls::result<urls::url_view> rv =
      urls::parse_relative_ref(req.target());
  if (not rv)
    return bad_request(rv.error().message(), std::move(req));

  if (not handlers_.count(rv->path()))
    return not_found_handler(std::move(req));

  return handlers_.at(rv->path())->handle(std::move(req));
}
