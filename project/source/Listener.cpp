#include "Listener.hpp"
#include <iostream>  // std::cerr

// Report a failure
void fail(beast::error_code ec, char const* what) {
  std::cerr << what << ": " << ec.message() << "\n";
}

void http_session::run() {
  net::dispatch(
      stream_.get_executor(),
      beast::bind_front_handler(&http_session::do_read, shared_from_this()));
}

void http_session::do_read() {
  // Make the request empty before reading,
  // otherwise the operation behavior is undefined.
  req_ = {};
  stream_.expires_after(std::chrono::seconds(timeout_));
  // Read a request
  http::async_read(
      stream_, buffer_, req_,
      beast::bind_front_handler(&http_session::on_read, shared_from_this()));
}

void http_session::on_read(beast::error_code ec,
                           std::size_t bytes_transferred) {
  boost::ignore_unused(bytes_transferred);

  // This means the connection was closed by client
  if (ec == http::error::end_of_stream)
    return do_close();

  if (ec)
    return fail(ec, "read");
  // Send back the response produced by handler
  send_response(handler_->handle(std::move(req_)));
}

void http_session::send_response(http::message_generator&& msg) {
  const bool keep_alive = msg.keep_alive();
  beast::async_write(stream_, std::move(msg),
                     beast::bind_front_handler(&http_session::on_write,
                                               shared_from_this(), keep_alive));
}

void http_session::on_write(bool keep_alive, beast::error_code ec,
                            std::size_t) {
  if (ec)
    return fail(ec, "write");

  // This indicates that we should close the socket, usually because
  // the response indicated the "Connection: close" semantic.
  if (not keep_alive)
    return do_close();

  // Read the next request from same socket
  do_read();
}

void http_session::do_close() {
  // Send a TCP shutdown
  beast::error_code ec;
  stream_.socket().shutdown(tcp::socket::shutdown_send, ec);
  // At this point the connection is closed gracefully
}

listener::listener(net::io_context& ioc, tcp::endpoint endpoint,
                   std::shared_ptr<Handler<> const> const& http_handler)
    : ioc_(ioc), acceptor_(net::make_strand(ioc)), handler_(http_handler) {
  beast::error_code ec;
  acceptor_.open(endpoint.protocol(), ec);
  if (ec) {
    fail(ec, "tcp open");
    return;
  }

  acceptor_.set_option(net::socket_base::reuse_address(true), ec);
  if (ec) {
    fail(ec, "tcp set option");
    return;
  }

  acceptor_.bind(endpoint, ec);
  if (ec) {
    fail(ec, "tcp bind");
    return;
  }

  acceptor_.listen(net::socket_base::max_listen_connections, ec);
  if (ec) {
    fail(ec, "tcp listen");
    return;
  }
}

void listener::run() {
  do_accept();
}

void listener::do_accept() {
  acceptor_.async_accept(
      net::make_strand(ioc_),
      beast::bind_front_handler(&listener::on_accept, shared_from_this()));
}

void listener::on_accept(beast::error_code ec, tcp::socket socket) {
  if (ec)
    return fail(ec, "accept");

  // Timeout value is hardcoded for now,
  // add an option later in the future maybe
  // Create the session and run it
  std::make_shared<http_session>(std::move(socket), handler_)->run();
  // Accept another connection
  do_accept();
}
