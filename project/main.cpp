#include <iostream>
#include "Listener.hpp"
#include "TodoHandler.hpp"
#include "TodoProvider.hpp"

int main(int argc, char const* argv[]) {
  // Check command line arguments.
  if (argc != 4) {
    std::cerr << "Usage: " << argv[0] << " <address> <port> <threads>\n"
              << "Example:\n"
              << "    " << argv[0] << " 0.0.0.0 8080 1\n";
    return EXIT_FAILURE;
  }

  auto const address = net::ip::make_address(argv[1]);
  auto const port = static_cast<std::uint16_t>(std::atoi(argv[2]));
  auto const threads = std::max<int>(1, std::atoi(argv[3]));

  // Create the data model and pass it to the controller (api)
  auto provider =
      InMemoryTodoProvider(std::unordered_map<std::string, TodoItem>());
  auto api = TodoHandler(provider);

  // TODO(sudotouchwoman): currently, the endpoint map has to be declared
  // using literals, which can be inconvenient later.
  // Using HttpHandler &, however, does not work somehow
  std::unordered_map<std::string, std::shared_ptr<HttpHandler>> handlers = {
      {"/todos", std::make_shared<HttpHandlerFunc>([&api](auto&& req) {
         return api.handleGetAllTodos(std::move(req));
       })},
      {"/my", std::make_shared<HttpHandlerFunc>([&api](auto&& req) {
         return api.handleGetTodosByAuthor(std::move(req));
       })},
      {"/recent", std::make_shared<HttpHandlerFunc>([&api](auto&& req) {
         return api.handleGetTodosAuthorRecent(std::move(req));
       })},
      {"/todo", std::make_shared<HttpHandlerFunc>([&api](auto&& req) {
         return api.handleGetTodoByID(std::move(req));
       })},
      {"/remove", std::make_shared<HttpHandlerFunc>([&api](auto&& req) {
         return api.handleRemoveTodoByID(std::move(req));
       })},
      {"/add", std::make_shared<HttpHandlerFunc>([&api](auto&& req) {
         return api.handleAddTodo(std::move(req));
       })},
  };

  // Create the IO context and start listening
  // for connections using the BasicRouter as a handler
  net::io_context ioc{threads};
  std::make_shared<listener>(ioc, tcp::endpoint{address, port},
                             std::make_shared<BasicRouter>(std::move(handlers)))
      ->run();

  // Capture signals to shutdown gracefully
  net::signal_set stop_signals(ioc, SIGINT, SIGTERM);
  stop_signals.async_wait([&ioc](beast::error_code const&, int) {
    // Stop the `io_context`. This will cause `run()`
    // to return immediately, eventually destroying the
    // `io_context` and all of the sockets in it.
    std::cout << "recieved iterrupt, signaling server to stop\n";
    ioc.stop();
  });

  // Run the I/O service on the requested number of threads
  std::vector<std::thread> v;
  v.reserve(threads - 1);
  for (auto i = threads - 1; i > 0; --i)
    v.emplace_back([&ioc] { ioc.run(); });
  ioc.run();

  // If execution reaches this point, we might have been interrupted
  for (auto& t : v)
    t.join();

  return EXIT_SUCCESS;
}
