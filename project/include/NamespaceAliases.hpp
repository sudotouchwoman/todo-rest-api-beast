#ifndef PROJECT_INCLUDE_NAMESPACEALIASES_HPP_
#define PROJECT_INCLUDE_NAMESPACEALIASES_HPP_

#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include <boost/beast/http.hpp>
#include <boost/json.hpp>

namespace beast = boost::beast;
namespace http = beast::http;
namespace net = boost::asio;
namespace json = boost::json;

#endif  // PROJECT_INCLUDE_NAMESPACEALIASES_HPP_
