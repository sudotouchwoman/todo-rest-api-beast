#ifndef PROJECT_INCLUDE_TODOPROVIDER_HPP_
#define PROJECT_INCLUDE_TODOPROVIDER_HPP_

#include <string>
#include <unordered_map>
#include <utility>  // std::move, std::pair
#include <vector>

#include "NamespaceAliases.hpp"
#include "TimeUtils.hpp"
// Date library docs
// https://howardhinnant.github.io/date/date.html

// Represents a todo entry at application side.
// Has an associated id, author and creation/update timestamps.
// Has an inner string content and a set of tags.
struct TodoItem {
  std::string id;
  std::string created_by;
  std::chrono::seconds created_at;
  std::chrono::seconds updated_at;
  std::string content;
  std::vector<std::string> tags;
  json::value asJson() const;
};

beast::error_code fromJson(TodoItem&, json::value const&);

static_assert(std::is_move_constructible<TodoItem>::value);
static_assert(std::is_copy_constructible<TodoItem>::value);

using TodoList = std::vector<TodoItem>;

// This interface provides access to the underlying
// data about todo entries. Implementations are
// assumed to provide thread-safe access and modification.
class TodoProvider {
 public:
  virtual ~TodoProvider();
  // Fetches all present todos
  virtual std::pair<TodoList, beast::error_code> allTodos() const = 0;
  // Fetches todos matching given id
  virtual std::pair<TodoItem, beast::error_code> todoByID(
      std::string const&) const = 0;
  // Fetches todos created by given author
  virtual std::pair<TodoList, beast::error_code> todosByAuthor(
      std::string const&) const = 0;
  // Fetches todos created by given author that are not older
  // than the given timestamp
  virtual std::pair<TodoList, beast::error_code> todosByAuthorRecent(
      std::string const&, std::chrono::seconds const) const = 0;
  // Create new todo entry
  virtual beast::error_code addTodo(TodoItem&&) = 0;
  // Remove todo entry by id
  virtual beast::error_code removeByID(std::string const&) = 0;
  // Remove todos created by given author
  virtual beast::error_code removeByAuthor(std::string const&) = 0;
};

// Represents a local (in-memory) todo provider.
// In such scenario, todos will only exist as long as
// the application is running.
class InMemoryTodoProvider : public TodoProvider {
 public:
  InMemoryTodoProvider() = delete;
  explicit InMemoryTodoProvider(std::unordered_map<std::string, TodoItem>&& map)
      : todos_map_(std::move(map)) {}
  std::pair<TodoList, beast::error_code> allTodos() const override;
  std::pair<TodoItem, beast::error_code> todoByID(
      std::string const&) const override;
  std::pair<TodoList, beast::error_code> todosByAuthor(
      std::string const&) const override;
  std::pair<TodoList, beast::error_code> todosByAuthorRecent(
      std::string const&, std::chrono::seconds const) const override;
  beast::error_code addTodo(TodoItem&&) override;
  beast::error_code removeByID(std::string const&) override;
  beast::error_code removeByAuthor(std::string const&) override;

 private:
  std::unordered_map<std::string, TodoItem> todos_map_;
  mutable std::mutex mu_;
};

#endif  // PROJECT_INCLUDE_TODOPROVIDER_HPP_
