#ifndef PROJECT_INCLUDE_LISTENER_HPP_
#define PROJECT_INCLUDE_LISTENER_HPP_

#include <memory>
#include <utility>

#include <boost/asio/ip/tcp.hpp>
#include "Handler.hpp"

using tcp = boost::asio::ip::tcp;  // from <boost/asio/ip/tcp.hpp>

// http_session is associated with a single tcp connection to
// the server. It reads incoming requests and writes responses.
class http_session : public std::enable_shared_from_this<http_session> {
 public:
  void run();
  http_session(tcp::socket&& socket,
               std::shared_ptr<Handler<> const> handler_map,
               int64_t timeout = 10)
      : stream_(std::move(socket)), handler_(handler_map), timeout_(timeout) {}

 private:
  beast::tcp_stream stream_;
  beast::flat_buffer buffer_;
  http::request<http::string_body> req_;
  std::shared_ptr<Handler<> const> handler_;
  const int64_t timeout_;

  void do_read();
  void on_read(beast::error_code ec, std::size_t bytes_transferred);
  void on_write(bool keep_alive, beast::error_code ec,
                std::size_t bytes_transferred);
  void do_close();
  void send_response(http::message_generator&& msg);
};

// listener represents a tcp server. It accepts incoming
// connections and creates http sessions.
class listener : public std::enable_shared_from_this<listener> {
 public:
  explicit listener(net::io_context& ioc, tcp::endpoint endpoint,
                    std::shared_ptr<Handler<> const> const& http_handler);
  void run();

 private:
  net::io_context& ioc_;
  tcp::acceptor acceptor_;
  std::shared_ptr<Handler<> const> handler_;

  void do_accept();
  void on_accept(beast::error_code ec, tcp::socket socket);
};

#endif  // PROJECT_INCLUDE_LISTENER_HPP_
