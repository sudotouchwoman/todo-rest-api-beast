#ifndef PROJECT_INCLUDE_TODOHANDLER_HPP_
#define PROJECT_INCLUDE_TODOHANDLER_HPP_

#include <memory>  // std::shared_ptr
#include <string>
#include <unordered_map>

#include "Handler.hpp"
#include "TodoProvider.hpp"

#define SERVER_VERSION "amogus"

// TodoHandler stores a reference to TodoProvider and
// uses it to implement business logic.
class TodoHandler {
 public:
  explicit TodoHandler(TodoProvider& provider) : provider_(provider) {}
  http::message_generator handleGetAllTodos(http::request<http::string_body>&&);
  http::message_generator handleGetTodoByID(http::request<http::string_body>&&);
  http::message_generator handleGetTodosByAuthor(
      http::request<http::string_body>&&);
  http::message_generator handleGetTodosAuthorRecent(
      http::request<http::string_body>&&);
  http::message_generator handleAddTodo(http::request<http::string_body>&&);
  http::message_generator handleRemoveTodoByID(
      http::request<http::string_body>&&);

 private:
  TodoProvider& provider_;
};

// BasicRouter stores a mapping from relative url
// paths and handlers. It redirects incoming requests
// to the underlying handlers.
class BasicRouter : public HttpHandler {
 public:
  explicit BasicRouter(
      std::unordered_map<std::string, std::shared_ptr<HttpHandler>>&& routes)
      : handlers_(routes) {}
  http::message_generator handle(
      http::request<http::string_body>&&) const override;

 private:
  const std::unordered_map<std::string, std::shared_ptr<HttpHandler>> handlers_;
};

// Replies with 404 `Page not found`
http::message_generator not_found_handler(http::request<http::string_body>&&);

#endif  // PROJECT_INCLUDE_TODOHANDLER_HPP_
