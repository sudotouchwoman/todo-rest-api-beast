#ifndef PROJECT_INCLUDE_TIMEUTILS_HPP_
#define PROJECT_INCLUDE_TIMEUTILS_HPP_

#include <string>
#include "date.hpp"
// Useful for debugging
// https://www.timestamp-converter.com/

// Converts time since epoch to ISO-string
std::string ts_to_isotime(std::chrono::seconds const& secs);
// Converts given ISO-string to seconds since epoch
std::chrono::seconds isotime_to_ts(std::string const& s);
// Check whether given string is ISO-format compliant
bool is_iso_string(std::string const& x);

#endif  // PROJECT_INCLUDE_TIMEUTILS_HPP_
