#ifndef PROJECT_INCLUDE_HANDLER_HPP_
#define PROJECT_INCLUDE_HANDLER_HPP_

#include <utility>  // std::move
#include "NamespaceAliases.hpp"

// Handler interface processes http requests
// and maps them to http responses
// (to boost::beast::http::message_generator type).
template <class Body = http::string_body,
          class Allocator = http::fields::allocator_type>
class Handler {
 public:
  virtual ~Handler() = 0;
  // Work on the provided request and produce a response.
  virtual http::message_generator handle(
      http::request<Body, http::basic_fields<Allocator>>&&) const = 0;
};

template <class Body, class Allocator>
Handler<Body, Allocator>::~Handler() {}

// Shorthand to omit extra brackets
using HttpHandler = Handler<>;

// Handler type is a function that maps request to
// a response.
template <class Body, class Allocator>
using LambdaHandler = std::function<http::message_generator(
    http::request<Body, http::basic_fields<Allocator>>&&)>;

// HandlerFunc is a Handler implementation that
// calls the provided function inside its handle method.
template <class Body = http::string_body,
          class Allocator = http::fields::allocator_type>
class HandlerFunc : public Handler<Body, Allocator> {
 public:
  explicit HandlerFunc(LambdaHandler<Body, Allocator>&& f) : func_(f) {}
  http::message_generator handle(
      http::request<Body, http::basic_fields<Allocator>>&& req) const override {
    return func_(std::move(req));
  }

 private:
  const LambdaHandler<Body, Allocator> func_;
};

// Shorthand without extra brackets pair
using HttpHandlerFunc = HandlerFunc<>;

#endif  // PROJECT_INCLUDE_HANDLER_HPP_
